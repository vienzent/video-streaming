@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if($directory != null)
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ action('AnimeController@index') }}">Home</a></li>
                    @php
                        $paths = '';
                    @endphp
                    @foreach(explode('/', $directory) as $path)

                        @if ($loop->last)
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ $path }}
                            </li>
                            @break
                        @endif
                        @php
                            $paths .= $path;
                        @endphp
                        <li class="breadcrumb-item"><a href="{{ action('AnimeController@index', base64_encode($paths)) }}">{{ $path }}</a></li>
                        @php
                            $paths .= '/';
                        @endphp
                    @endforeach
                    
                </ol>
            </nav>
            @endif
            <div class="list-group">
                @foreach ($directories as $dir)
                    <a href="{{ action('AnimeController@index', base64_encode($dir)) }}" class="list-group-item list-group-item-action">
                        <i class="fa fa-folder text-info"></i> {{ collect(explode('/',$dir))->last() }}
                    </a>
                @endforeach
                @foreach ($files as $file)
                    <a href="{{ action('AnimeController@watch', base64_encode($file)) }}" class="list-group-item list-group-item-action">
                        <i class="fa fa-file-video-o text-success"></i> {{ collect(explode('/',$file))->last() }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection