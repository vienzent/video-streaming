@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
 
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ action('AnimeController@index') }}">Home</a></li>
                    @php
                        $paths = '';
                    @endphp
                    @foreach(explode('/', $file_decoded) as $path)

                        @if ($loop->last)
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ $path }}
                            </li>
                            @break
                        @endif
                        @php
                            $paths .= $path;
                        @endphp
                        <li class="breadcrumb-item"><a href="{{ action('AnimeController@index', base64_encode($paths)) }}">{{ $path }}</a></li>
                        @php
                            $paths .= '/';
                        @endphp
                    @endforeach
                    
                </ol>
            </nav>

            
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- 21:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-21by9">
                <video controls class="embed-responsive-item">
                    <source src="{{ action('AnimeController@file', $file) }}" type="{{ $mimeType }}">

                Your browser does not support the video tag.
                </video>
            </div>
        </div>
    </div>
</div>
@endsection