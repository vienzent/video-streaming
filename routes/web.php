<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('tutorials/{directory?}', 'TutorialsController@index');
Route::get('tutorials/{file}/watch', 'TutorialsController@watch');
Route::get('tutorials/{file}/file', 'TutorialsController@file');

Route::get('biloy/{directory?}', 'BiloyController@index');
Route::get('biloy/{file}/watch', 'BiloyController@watch');
Route::get('biloy/{file}/file', 'BiloyController@file');

Route::get('nsfw/{directory?}', 'NsfwController@index');
Route::get('nsfw/{file}/watch', 'NsfwController@watch');
Route::get('nsfw/{file}/file', 'NsfwController@file');

Route::get('anime/{directory?}', 'AnimeController@index');
Route::get('anime/{file}/watch', 'AnimeController@watch');
Route::get('anime/{file}/file', 'AnimeController@file');

Auth::routes();

Route::get('/', 'CommonController@home')->name('home');
