<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class TutorialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($directory = null)
    {
        if($directory != null) {
            $directory = base64_decode($directory);
        }
        
        if($directory != null && !Storage::disk('tutorials')->exists($directory)) {
            return app()->abort(404);
        }

        $directories = Storage::disk('tutorials')->directories($directory);
        $files = collect(Storage::disk('tutorials')->files($directory))
            ->filter(function ($file, $key) {
                return in_array(strtoupper(pathinfo($file, PATHINFO_EXTENSION)), ['MP4', 'OGG' ,'WEBM']);
            });

        return view('tutorials.index', compact('directories', 'directory', 'files'));
    }

    public function watch($file)
    {
        $file_decoded = base64_decode($file);

        if (!Storage::disk('tutorials')->exists($file_decoded) && in_array(strtoupper(pathinfo($file_decoded, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            return app()->abort(404);
        }
        $mimeType = Storage::disk('tutorials')->mimeType($file_decoded);
        return view('tutorials.watch', compact('file', 'file_decoded', 'mimeType'));
    }

    public function file($file)
    {
    
        $file = base64_decode($file);

        if (!Storage::disk('tutorials')->exists($file)) {
            return app()->abort(404);
        }

        $stream = new \App\Http\VideoStream(Storage::disk('tutorials')->getDriver(), $file);
 
        return response()->stream(function () use ($stream) {
            $stream->start();
        });
    }

}
