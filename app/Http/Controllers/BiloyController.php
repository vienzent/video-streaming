<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class BiloyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($directory = null)
    {
        if ($directory != null) {
            $directory = base64_decode($directory);
        }

        if ($directory != null && !Storage::disk('torrents')->exists($directory)) {
            return app()->abort(404);
        }

        $directories = Storage::disk('torrents')->directories($directory);
        $files = collect(Storage::disk('torrents')->files($directory));

        return view('biloy.index', compact('directories', 'directory', 'files'));
    }

    public function watch($file)
    {
        $file_decoded = base64_decode($file);

        if (!Storage::disk('torrents')->exists($file_decoded)) {
            return app()->abort(404);
        }

        if(in_array(strtoupper(pathinfo($file_decoded, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            $mimeType = Storage::disk('torrents')->mimeType($file_decoded);
            return view('biloy.watch', compact('file', 'file_decoded', 'mimeType'));
        }

        return Storage::disk('torrents')->download($file_decoded);
    }

    public function file($file)
    {
        $file = base64_decode($file);

        if (!Storage::disk('torrents')->exists($file)) {
            return app()->abort(404);
        }

        $stream = new \App\Http\VideoStream(Storage::disk('torrents')->getDriver(), $file);

        return response()->stream(function () use ($stream) {
            $stream->start();
        });


        $size = Storage::disk('torrents')->size($file);
        $stream = Storage::disk('torrents')->getDriver()->read($file);

        $type = Storage::disk('torrents')->mimeType($file);
        $start = 0;
        $length = $size;
        $status = 200;

        $headers = ['Content-Type' => $type, 'Content-Length' => $size, 'Accept-Ranges' => 'bytes'];

        if (false !== $range = Request::server('HTTP_RANGE', false)) {
            list($param, $range) = explode('=', $range);
            if (strtolower(trim($param)) !== 'bytes') {
                header('HTTP/1.1 400 Invalid Request');
                exit;
            }
            list($from, $to) = explode('-', $range);
            if ($from === '') {
                $end = $size - 1;
                $start = $end - intval($from);
            } elseif ($to === '') {
                $start = intval($from);
                $end = $size - 1;
            } else {
                $start = intval($from);
                $end = intval($to);
            }
            $length = $end - $start + 1;
            $status = 206;
            $headers['Content-Range'] = sprintf('bytes %d-%d/%d', $start, $end, $size);
        }

        return response()->stream(function () use ($stream, $start, $length) {
            fseek($stream, $start, SEEK_SET);
            echo fread($stream, $length);
            fclose($stream);
        }, $status, $headers);

        //return Storage::disk('torrents')->response($file, null, []);
    }

}
