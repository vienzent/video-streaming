<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class AnimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($directory = null)
    {
        if ($directory != null) {
            $directory = base64_decode($directory);
        }

        if ($directory != null && !Storage::disk('anime')->exists($directory)) {
            return app()->abort(404);
        }

        $directories = Storage::disk('anime')->directories($directory);
        $files = collect(Storage::disk('anime')->files($directory));

        return view('anime.index', compact('directories', 'directory', 'files'));
    }

    public function watch($file)
    {
        $file_decoded = base64_decode($file);

        if (!Storage::disk('anime')->exists($file_decoded)) {
            return app()->abort(404);
        }

        if (in_array(strtoupper(pathinfo($file_decoded, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            $mimeType = Storage::disk('anime')->mimeType($file_decoded);
            return view('anime.watch', compact('file', 'file_decoded', 'mimeType'));
        }

        return Storage::disk('anime')->download($file_decoded);
    }

    public function file($file)
    {
        $file = base64_decode($file);

        if (!Storage::disk('anime')->exists($file)) {
            return app()->abort(404);
        }

        $stream = new \App\Http\VideoStream(Storage::disk('anime')->getDriver(), $file);

        if (in_array(strtoupper(pathinfo($file, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            return response()->stream(function () use ($stream) {
                $stream->start();
            });
        }

        return Storage::disk('anime')->download($file);
    }
}
