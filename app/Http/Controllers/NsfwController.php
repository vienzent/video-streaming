<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class NsfwController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($directory = null)
    {
        if ($directory != null) {
            $directory = base64_decode($directory);
        }

        if ($directory != null && !Storage::disk('nsfw')->exists($directory)) {
            return app()->abort(404);
        }

        $directories = Storage::disk('nsfw')->directories($directory);
        $files = collect(Storage::disk('nsfw')->files($directory));

        return view('nsfw.index', compact('directories', 'directory', 'files'));
    }

    public function watch($file)
    {
        $file_decoded = base64_decode($file);

        if (!Storage::disk('nsfw')->exists($file_decoded)) {
            return app()->abort(404);
        }

        if (in_array(strtoupper(pathinfo($file_decoded, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            $mimeType = Storage::disk('nsfw')->mimeType($file_decoded);
            return view('nsfw.watch', compact('file', 'file_decoded', 'mimeType'));
        }

        return Storage::disk('nsfw')->download($file_decoded);
    }

    public function file($file)
    {
        $file = base64_decode($file);

        if (!Storage::disk('nsfw')->exists($file)) {
            return app()->abort(404);
        }

        $stream = new \App\Http\VideoStream(Storage::disk('nsfw')->getDriver(), $file);

        if (in_array(strtoupper(pathinfo($file, PATHINFO_EXTENSION)), ['MP4', 'OGG', 'WEBM'])) {
            return response()->stream(function () use ($stream) {
                $stream->start();
            });
        }

        return Storage::disk('nsfw')->download($file);
    }

}
